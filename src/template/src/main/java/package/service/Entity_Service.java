package {package_name}.service;

public interface {entity_name}Service {
    void save({entity_name}Dto {entity_name_lower}Dto);
    void update({entity_name}Dto {entity_name_lower}Dto);
    void delete(Long id);
    List<{entity_name}Dto> findAll();
    {entity_name}Dto findById(Long id);
}