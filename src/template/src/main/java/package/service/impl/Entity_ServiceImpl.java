package {package_name}.service.impl;

import {package_name}.service.{entity_name}Service;
import org.springframework.stereotype.Service;

@Service
public class {entity_name}ServiceImpl implements {entity_name}Service {
}