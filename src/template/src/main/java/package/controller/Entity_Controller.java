package com.facturapp.controller;

import org.springframework.web.bind.annotation.*;
import com.facturapp.service.dto.{entity_name}Dto;

@RestController
@RequestMapping("/api/company")
public class {entity_name}Controller {
    private final {entity_name}Service {entity_name}Service;

    public {entity_name}Controller({entity_name}Service {entity_name_lowercase}Service) {
        this.{entity_name}Service = {entity_name}Service;
    }

    @PostMapping
    public ResponseEntity<{entity_name}Dto> save(@RequestBody {entity_name}Dto {entity_name_lowercase}Dto) {
        var result = {entity_name}Service.save({entity_name_lowercase}Dto);
        return ResponseEntity.ok(result);
    }
}