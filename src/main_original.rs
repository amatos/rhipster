mod main;

use std::fs::{create_dir_all, write, read_to_string};
use std::io::{self, Error, Write};
use std::process::Command;
use regex::Regex;
use clap::Parser;

/// Rust application to generate a Java Spring Boot application using Maven
#[derive(Parser)]
struct Cli {}

/// Main function to drive the application
fn main() {
    let _args = Cli::parse();
    let app_name = prompt("Enter the application name: ");
    let package_name = prompt("Enter the package name: ");
    let entity_name = prompt_entity_name("Enter the entity name: ");
    let entity_fields = prompt_entity_fields();

    generate_project_structure(&app_name, &package_name, &entity_name, &entity_fields);
    build_maven_project(&app_name);

    println!("Successfully created Spring Boot application: {}", app_name);
}

/// Prompts the user for input with a specified message and returns the trimmed string input.
/// If the input is empty, it displays an error and prompts again.
///
/// # Arguments
/// * `message` - The message to prompt the user with.
///
/// # Returns
/// A valid, non-empty string entered by the user.
fn prompt(message: &str) -> String {
    loop {
        print!("{}", message);
        io::stdout().flush().unwrap();

        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();
        let trimmed = input.trim();

        if !trimmed.is_empty() {
            return trimmed.to_string();
        } else {
            eprintln!("Error: Input cannot be empty. Please try again.");
        }
    }
}

/// Prompts the user for the entity name and ensures it is in CamelCase format.
///
/// # Arguments
/// * `message` - The message to prompt the user with.
///
/// # Returns
/// A valid CamelCase entity name entered by the user.
fn prompt_entity_name(message: &str) -> String {
    loop {
        let entity_name = prompt(message);
        if is_valid_entity_name(&entity_name) {
            return entity_name;
        } else {
            eprintln!("Error: Entity name must be in CamelCase. Please try again.");
        }
    }
}

/// Validates if the given entity name is in CamelCase format.
///
/// # Arguments
/// * `name` - The entity name to validate.
///
/// # Returns
/// `true` if the name is in CamelCase, `false` otherwise.
fn is_valid_entity_name(name: &str) -> bool {
    Regex::new(r"^[A-Z][a-zA-Z0-9]*$").unwrap().is_match(name)
}

/// Prompts the user for fields of the entity, including field name and type.
/// Loops until the user decides not to add more fields.
///
/// # Arguments
/// * `entity_name` - The name of the entity for which fields are being added.
///
/// # Returns
/// A vector of tuples containing field names and types.
fn prompt_entity_fields() -> Vec<(String, String)> {
    let mut fields = Vec::new();

    loop {
        let field_name = prompt_field_name("Enter the field name: ");
        let field_type = prompt_field_type("Enter the field type: ");
        fields.push((field_name, field_type));

        let add_more = prompt("Do you want to add more fields? (yes/no) [default: yes]: ");
        if !is_positive_response(&add_more) {
            break;
        }
    }

    fields
}

/// Checks if the response indicates a positive answer.
///
/// # Arguments
/// * `response` - The user response to check.
///
/// # Returns
/// `true` if the response indicates 'yes', `false` otherwise.
fn is_positive_response(response: &str) -> bool {
    matches!(response.to_lowercase().as_str(), "" | "yes" | "y")
}

/// Prompts the user for the field name and ensures it is in camelCase format.
///
/// # Arguments
/// * `message` - The message to prompt the user with.
///
/// # Returns
/// A valid camelCase field name entered by the user.
fn prompt_field_name(message: &str) -> String {
    loop {
        let field_name = prompt(message);
        if is_valid_field_name(&field_name) {
            return field_name;
        } else {
            eprintln!("Error: Field name must be camelCase and start with a lowercase letter. Please try again.");
        }
    }
}

/// Validates if the given field name is in camelCase format.
///
/// # Arguments
/// * `name` - The field name to validate.
///
/// # Returns
/// `true` if the name is in camelCase, `false` otherwise.
fn is_valid_field_name(name: &str) -> bool {
    Regex::new(r"^[a-z][a-zA-Z0-9]*$").unwrap().is_match(name)
}

/// Prompts the user for the field type and ensures it is a valid Java type.
///
/// # Arguments
/// * `message` - The message to prompt the user with.
///
/// # Returns
/// A valid Java field type entered by the user.
fn prompt_field_type(message: &str) -> String {
    loop {
        let field_type = prompt(message);
        if is_valid_field_type(&field_type) {
            return field_type;
        } else {
            eprintln!("Error: Invalid Java field type. Please try again.");
        }
    }
}

/// Validates if the given field type is a common Java type.
///
/// # Arguments
/// * `field_type` - The field type to validate.
///
/// # Returns
/// `true` if the field type is a common Java type, `false` otherwise.
fn is_valid_field_type(field_type: &str) -> bool {
    // A simplified check for common Java types
    let valid_types = ["String", "int", "Integer", "long", "Long", "float", "Float", "double", "Double", "boolean", "Boolean", "Date"];
    valid_types.contains(&field_type)
}

/// Generates the project structure and files for the Spring Boot application.
///
/// # Arguments
/// * `app_name` - The name of the application.
/// * `package_name` - The base package name for the application.
/// * `entity_name` - The name of the entity.
/// * `entity_fields` - A vector of tuples containing field names and types.
fn generate_project_structure(app_name: &str, package_name: &str, entity_name: &str, entity_fields: &[(String, String)]) {
    let base_dir = format!("{}/src/main/java/{}", app_name, package_name.replace(".", "/"));
    println!("base_dir, {}!", base_dir);
    create_dir_all(&base_dir).expect("Failed to create directory structure");

    let domain_dir = format!("{}/domain", base_dir);
    create_dir_all(&domain_dir).expect("Failed to create domain directory");

    let entity_path = format!("{}/{}.java", domain_dir, entity_name);
    generate_entity_file_from_template("src/template/src/main/java/package/domain/Entity.java", &entity_path, package_name, entity_name, entity_fields);

    let repository_path = format!("{}/repository/{}Repository.java", base_dir, entity_name);
    create_dir_all(format!("{}/repository", base_dir)).expect("Failed to create repository directory");
    generate_file_from_template("src/template/src/main/java/package/repository/Entity_Repository.java", &repository_path, package_name, entity_name);

    let service_path = format!("{}/service/{}Service.java", base_dir, entity_name);
    create_dir_all(format!("{}/service", base_dir)).expect("Failed to create service directory");
    generate_file_from_template("src/template/src/main/java/package/service/Entity_Service.java", &service_path, package_name, entity_name);

    let controller_path = format!("{}/controller/{}Controller.java", base_dir, entity_name);
    create_dir_all(format!("{}/controller", base_dir)).expect("Failed to create controller directory");
    generate_file_from_template("src/template/src/main/java/package/controller/Entity_Controller.java", &controller_path, package_name, entity_name);

    generate_pom(app_name, package_name, &format!("{}/pom.xml", app_name));
}

fn generate_entity_file_from_template(template_path: &str, destination_path: &str, package_name: &str, entity_name: &str, entity_fields: &[(String, String)]) {
    println!("entity: {}", template_path);
    let content = read_to_string(template_path).expect("Failed to read template file");
    let content = content.replace("{package_name}", package_name);
    let content = content.replace("{entity_name}", entity_name);
    let content = content.replace("{entity_name_lower}", &entity_name.to_ascii_lowercase());
    // If there are other placeholders in the template, replace them here...
    generate_fields_from_template(template_path, destination_path, package_name, entity_name, entity_fields).expect("Failed to generate fields");
    write(destination_path, content).expect("Failed to write file");
}

fn generate_file_from_template(template_path: &str, destination_path: &str, package_name: &str, entity_name: &str) {
    println!("template_path, {}", template_path);
    let content = read_to_string(template_path).expect("Failed to read template file");
    let content = content.replace("{package_name}", package_name);
    let content = content.replace("{entity_name}", entity_name);
    let content = content.replace("{entity_name_lower}", &entity_name.to_ascii_lowercase());
    // If there are other placeholders in the template, replace them here...

    write(destination_path, content).expect("Failed to write file");
}

fn generate_pom(app_name: &str, package_name: &str, destination_path: &str) {
    let pom_path = "src/template/pom.xml";
    let content = read_to_string(pom_path).expect("Failed to read template file");
    let content = content.replace("{package_name}", package_name);
    let content = content.replace("{app_name}", app_name);
    write(destination_path, content).expect("Failed to write file");
}

fn generate_fields_from_template(
    template_path: &str,
    destination_path: &str,
    package_name: &str,
    entity_name: &str,
    entity_fields: &[(String, String)]
) -> Result<(), Error> {
    let mut fields = String::new();
    let mut getters_and_setters = String::new();

    for (field_name, field_type) in entity_fields {
        let column_name = camel_to_snake_case(field_name);
        fields.push_str(&format!("@Column(\"{}\")", column_name));
        fields.push_str(&format!("private {} {};\n", field_type, field_name));

        let getter = format!(
            "public {} get{}() {{\nreturn {};\n}}\n",
            field_type,
            field_name.chars().next().unwrap().to_uppercase().to_string() + &field_name[1..],
            field_name
        );

        let setter = format!(
            "public void set{}({} {}) {{\nthis.{} = {};\n}}\n",
            field_name.chars().next().unwrap().to_uppercase().to_string() + &field_name[1..],
            field_type,
            field_name,
            field_name,
            field_name
        );

        getters_and_setters.push_str(&getter);
        getters_and_setters.push_str(&setter);
    }

    let content = read_to_string(template_path)?;
    let content = content.replace("{package_name}", package_name);
    let content = content.replace("{entity_name}", entity_name);
    let content = content.replace("{entity_name_lower}", &entity_name.to_ascii_lowercase());
    let content = content.replace("{fields}", &fields);
    let content = content.replace("{getters_and_setters}", &getters_and_setters);

    write(destination_path, content)
}

fn camel_to_snake_case(s: &str) -> String {
    let re = Regex::new(r"(?<=\p{Lowercase})(\p{Uppercase})").unwrap();
    let s = re.replace_all(s, "_$1");
    s.to_lowercase()
}

/// Builds the Maven project by running `mvn clean install`.
///
/// # Arguments
/// * `app_name` - The name of the application.
fn build_maven_project(app_name: &str) {
    Command::new("mvn")
        .args(&["clean", "install"])
        .current_dir(app_name)
        .status()
        .expect("Failed to build Maven project");
}
