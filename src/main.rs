use std::collections::HashMap;
use std::fs::{create_dir_all, read_to_string, write};
use std::io::{self, Write};
use regex::Regex;

fn main() {
    // Prompt for application name
    let app_name = loop {
        let name = prompt("Enter the application name: ");
        if !name.is_empty() {
            break name;
        }
        eprintln!("\x1b[31mError: Application name cannot be empty. Please try again.\x1b[0m");
    };

    // Prompt for application package
    let package_name = loop {
        let package = prompt("Enter the application package: ");
        if !package.is_empty() {
            if !is_valid_package_name(&package) {
                let confirmed = ask_yes_no("The package name is not in correct format. Do you want to use the suggested format? (yes/no) [default: yes]: ");
                if confirmed {
                    let formatted = format_package_name(&package);
                    println!("Package name adjusted to: {}", formatted);
                    break formatted;
                }
            } else {
                break package;
            }
        }
        eprintln!("\x1b[31mError: Package name cannot be empty. Please try again.\x1b[0m");
    };

    // Prompt for entity creation
    let create_entity = ask_yes_no("Do you want to create an entity? (yes/no) [default: yes]: ");

    let mut entities = Vec::new();

    if create_entity {
        loop {
            // Prompt for entity name
            let entity_name = loop {
                let name = prompt("Enter the entity name: ");
                if !name.is_empty() {
                    break name;
                }
                eprintln!("\x1b[31mError: Entity name cannot be empty. Please try again.\x1b[0m");
            };

            // Collect fields for the entity
            let mut fields = Vec::new();
            loop {
                // Prompt for field name
                let field_name = loop {
                    let name = prompt("Enter the field name: ");
                    if !name.is_empty() && is_camel_case(&name) {
                        break name;
                    }
                    eprintln!("\x1b[31mError: Field name must be in camelCase. Please try again.\x1b[0m");
                };

                // Prompt for field type using combo-box style selection
                let field_type = prompt_field_type();

                // Collect restrictions based on field type
                let restrictions = collect_restrictions(&field_type);

                fields.push((field_name, field_type, restrictions));

                // Ask if the user wants to add more fields
                let add_more = ask_yes_no("Do you want to add more fields? (yes/no) [default: yes]: ");
                if !add_more {
                    break;
                }
            }

            entities.push((entity_name, fields));

            // Ask if the user wants to create another entity
            let create_another = ask_yes_no("Do you want to create another entity? (yes/no) [default: yes]: ");
            if !create_another {
                break;
            }
        }
    }

    // Generate the project structure and files
    generate_project(&app_name, &package_name, &entities);
}

fn prompt(message: &str) -> String {
    print!("\x1b[32m{}\x1b[0m", message);
    io::stdout().flush().unwrap();

    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.trim().to_string()
}

fn is_valid_package_name(package: &str) -> bool {
    let camel_case = Regex::new(r"^[a-z][a-zA-Z0-9]*$").unwrap();
    let snake_case = Regex::new(r"^[a-z][a-z0-9_]*$").unwrap();
    package.split('.').all(|part| camel_case.is_match(part) || snake_case.is_match(part))
}

fn format_package_name(package: &str) -> String {
    let package = package.replace('_', ".");
    let re = Regex::new(r"([a-z])([A-Z])").unwrap();
    let package = re.replace_all(&package, "$1.$2");
    package.to_lowercase()
}

fn is_camel_case(name: &str) -> bool {
    let re = Regex::new(r"^[a-z][a-zA-Z0-9]*$").unwrap();
    re.is_match(name)
}

fn prompt_field_type() -> String {
    let field_types = [
        "String", "Integer", "Long", "BigDecimal", "Float", "Double", "Enum", "Boolean", "LocalDate",
        "ZonedDateTime", "Instant", "Duration", "UUID", "Blob", "AnyBlob", "ImageBlob", "TextBlob"
    ];
    loop {
        println!("\x1b[32mSelect a field type:\x1b[0m");
        for (i, field_type) in field_types.iter().enumerate() {
            println!("\x1b[32m[{}] {}\x1b[0m", i + 1, field_type);
        }
        let choice = prompt("Enter the number corresponding to the field type: ");
        if let Ok(index) = choice.parse::<usize>() {
            if index >= 1 && index <= field_types.len() {
                return field_types[index - 1].to_string();
            }
        }
        eprintln!("\x1b[31mError: Invalid selection. Please enter a number between 1 and {}.\x1b[0m", field_types.len());
    }
}

fn collect_restrictions(field_type: &str) -> HashMap<String, String> {
    let mut restrictions = HashMap::new();
    match field_type {
        "String" => {
            if ask_yes_no("Is the field required? (yes/no) [default: yes]: ") {
                restrictions.insert("required".to_string(), "true".to_string());
            }
            if let Some(minlength) = prompt_optional_number("Enter minlength (or press Enter to skip): ") {
                restrictions.insert("minlength".to_string(), minlength);
            }
            if let Some(maxlength) = prompt_optional_number("Enter maxlength (or press Enter to skip): ") {
                restrictions.insert("maxlength".to_string(), maxlength);
            }
            if let Some(pattern) = prompt_optional_string("Enter pattern (or press Enter to skip): ") {
                restrictions.insert("pattern".to_string(), pattern);
            }
            if ask_yes_no("Should the field be unique? (yes/no) [default: no]: ") {
                restrictions.insert("unique".to_string(), "true".to_string());
            }
        }
        "Integer" | "Long" | "BigDecimal" | "Float" | "Double" => {
            if ask_yes_no("Is the field required? (yes/no) [default: yes]: ") {
                restrictions.insert("required".to_string(), "true".to_string());
            }
            if let Some(min) = prompt_optional_number("Enter min (or press Enter to skip): ") {
                restrictions.insert("min".to_string(), min);
            }
            if let Some(max) = prompt_optional_number("Enter max (or press Enter to skip): ") {
                restrictions.insert("max".to_string(), max);
            }
            if ask_yes_no("Should the field be unique? (yes/no) [default: no]: ") {
                restrictions.insert("unique".to_string(), "true".to_string());
            }
        }
        "Enum" | "Boolean" | "LocalDate" | "ZonedDateTime" | "Instant" | "Duration" | "UUID" | "TextBlob" => {
            if ask_yes_no("Is the field required? (yes/no) [default: yes]: ") {
                restrictions.insert("required".to_string(), "true".to_string());
            }
            if ask_yes_no("Should the field be unique? (yes/no) [default: no]: ") {
                restrictions.insert("unique".to_string(), "true".to_string());
            }
        }
        "Blob" | "AnyBlob" | "ImageBlob" => {
            if ask_yes_no("Is the field required? (yes/no) [default: yes]: ") {
                restrictions.insert("required".to_string(), "true".to_string());
            }
            if let Some(minbytes) = prompt_optional_number("Enter minbytes (or press Enter to skip): ") {
                restrictions.insert("minbytes".to_string(), minbytes);
            }
            if let Some(maxbytes) = prompt_optional_number("Enter maxbytes (or press Enter to skip): ") {
                restrictions.insert("maxbytes".to_string(), maxbytes);
            }
            if ask_yes_no("Should the field be unique? (yes/no) [default: no]: ") {
                restrictions.insert("unique".to_string(), "true".to_string());
            }
        }
        _ => {}
    }
    restrictions
}

fn prompt_optional_number(message: &str) -> Option<String> {
    loop {
        let input = prompt(message);
        if input.is_empty() {
            return None;
        }
        if input.parse::<f64>().is_ok() {
            return Some(input);
        }
        eprintln!("\x1b[31mError: Please enter a valid number.\x1b[0m");
    }
}

fn prompt_optional_string(message: &str) -> Option<String> {
    let input = prompt(message);
    if input.is_empty() {
        None
    } else {
        Some(input)
    }
}

fn ask_yes_no(message: &str) -> bool {
    let response = prompt(message);
    matches!(
        response.to_lowercase().as_str(),
        "" | "yes" | "y" | "ye"
    )
}

fn generate_project(app_name: &str, package_name: &str, entities: &[(String, Vec<(String, String, HashMap<String, String>)>)]) {
    let base_path = format!("{}/src/main/java/{}", app_name, package_name.replace('.', "/"));

    // Create domain package and entities
    let domain_path = format!("{}/domain", base_path);
    create_dir_all(&domain_path).unwrap();

    for (entity_name, fields) in entities {
        let class_file_path = format!("{}/{}.java", domain_path, entity_name);
        let mut field_strings = Vec::new();

        for (field_name, field_type, restrictions) in fields {
            let mut annotations = Vec::new();
            if let Some(required) = restrictions.get("required") {
                if required == "true" {
                    annotations.push("@NotNull".to_string());
                }
            }
            match field_type.as_str() {
                "String" => {
                    if let Some(minlength) = restrictions.get("minlength") {
                        annotations.push(format!("@Size(min = {})", minlength));
                    }
                    if let Some(maxlength) = restrictions.get("maxlength") {
                        annotations.push(format!("@Size(max = {})", maxlength));
                    }
                    if let Some(pattern) = restrictions.get("pattern") {
                        annotations.push(format!(r#"@Pattern(regexp = "{}")"#, pattern));
                    }
                }
                "Integer" | "Long" | "BigDecimal" | "Float" | "Double" => {
                    if let Some(min) = restrictions.get("min") {
                        annotations.push(format!("@Min(value = {})", min));
                    }
                    if let Some(max) = restrictions.get("max") {
                        annotations.push(format!("@Max(value = {})", max));
                    }
                }
                "Blob" | "AnyBlob" | "ImageBlob" => {
                    if let Some(minbytes) = restrictions.get("minbytes") {
                        annotations.push(format!("@Min(value = {})", minbytes));
                    }
                    if let Some(maxbytes) = restrictions.get("maxbytes") {
                        annotations.push(format!("@Max(value = {})", maxbytes));
                    }
                }
                _ => {}
            }
            if let Some(unique) = restrictions.get("unique") {
                if unique == "true" {
                    annotations.push("@Column(unique = true)".to_string());
                }
            }

            let annotations_string = annotations.join("\n    ");
            field_strings.push(format!("    {}\n    private {} {};", annotations_string, field_type, field_name));
        }

        let fields_string = field_strings.join("\n\n");

        let entity_template = read_to_string("src/templates/Entity.java.template").unwrap();
        let entity_content = entity_template
            .replace("{{package_name}}", package_name)
            .replace("{{entity_name}}", entity_name)
            .replace("{{entity_name_snake}}", &to_snake_case(&entity_name))
            .replace("{{fields}}", &fields_string);

        write(class_file_path, entity_content).unwrap();
    }

    // Generate repository, service, and controller from templates
    let repository_path = format!("{}/repository", base_path);
    create_dir_all(&repository_path).unwrap();

    let service_path = format!("{}/service", base_path);
    create_dir_all(&service_path).unwrap();

    let controller_path = format!("{}/controller", base_path);
    create_dir_all(&controller_path).unwrap();

    for (entity_name, _) in entities {
        // Repository
        let repository_template = read_to_string("src/templates/Repository.java.template").unwrap();
        let repository_content = repository_template
            .replace("{{package_name}}", package_name)
            .replace("{{entity_name}}", entity_name);
        let repository_file_path = format!("{}/{}Repository.java", repository_path, entity_name);
        write(repository_file_path, repository_content).unwrap();

        // Service
        let service_template = read_to_string("src/templates/Service.java.template").unwrap();
        let service_content = service_template
            .replace("{{package_name}}", package_name)
            .replace("{{entity_name}}", entity_name);
        let service_file_path = format!("{}/{}Service.java", service_path, entity_name);
        write(service_file_path, service_content).unwrap();

        // Controller
        let controller_template = read_to_string("src/templates/Controller.java.template").unwrap();
        let controller_content = controller_template
            .replace("{{package_name}}", package_name)
            .replace("{{entity_name}}", entity_name);
        let controller_file_path = format!("{}/{}Controller.java", controller_path, entity_name);
        write(controller_file_path, controller_content).unwrap();
    }

    // Generate pom.xml from template
    let pom_template = read_to_string("src/templates/pom.xml.template").unwrap();
    let pom_content = pom_template
        .replace("{{app_name}}", app_name);
    write(format!("{}/pom.xml", app_name), pom_content).unwrap();

    println!("\x1b[32mProject generated successfully.\x1b[0m");
}

fn to_snake_case(input: &str) -> String {
    let mut snake_case = String::new();
    for (i, c) in input.chars().enumerate() {
        if i == 0 {
            snake_case.push(c.to_ascii_lowercase());
        } else if c.is_uppercase() {
            snake_case.push('_');
            snake_case.push(c.to_ascii_lowercase());
        } else {
            snake_case.push(c);
        }
    }
    snake_case
}